﻿using NOSWebApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NOSWebApp.ViewModels
{
    public class SeasonStatsView
    {
        public virtual Season Season { get; set; }
        public List<ClubSeasonStats> ClubSeasonStats { get; set; }
        public int TotalFixtures { get; set; }
        public int TotalGameMatches { get; set; }
        public int TotalGoalsScored { get; set; }

        public SeasonBets SeasonBets { get; set; }
    }
}