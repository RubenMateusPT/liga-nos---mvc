﻿using NOSWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NOSWebApp.ViewModels
{
    public class FixtureStatsView
    {
        public Fixture Fixture { get; set; }

        public int TotalGameMatches { get; set; }
        public int TotalScoredGoals { get; set; }
        public Club ClubMostGoalsScored { get; set; }
        public Club ClubMostGoalsConceded { get; set; }
    }
}