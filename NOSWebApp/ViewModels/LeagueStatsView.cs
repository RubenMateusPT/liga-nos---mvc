﻿using NOSWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NOSWebApp.ViewModels
{
    public class LeagueStatsView
    {
        public League League { get; set; }

        public int TotalSeason { get; set; }
        public int TotalFixtures { get; set; }
        public int TotalGameMatches { get; set; }
        public int TotalGoalsScored { get; set; }

        public Club ClubMostVictories { get; set; }
        public Club ClubMostDefeats { get; set; }
        public Club ClubMostGoalsScored { get; set; }
        public Club ClubMostGoalsConceded { get; set; }
    }
}