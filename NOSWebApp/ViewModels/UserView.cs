﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NOSWebApp.ViewModels
{
    public class UserView
    {
        public string UserID { get; set; }
        public string ProfilePic { get; set; }
        public string Username { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string FullName { get; set; }

        public DateTime? Birthday { get; set; }

        public RoleView Role { get; set; }

        public List<RoleView> Roles { get; set; }
    }
}
