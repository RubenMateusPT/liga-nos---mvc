﻿using NOSWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NOSWebApp.ViewModels
{
    public class ClubStatsView
    {
        public Club Club { get; set; }

        public int BestClassification { get; set; }
        public Season BestClassificationSeason { get; set; }

        public int BestPoints { get; set; }
        public Season BestPointsMadeSeason { get; set; }

        public int WorstClassification { get; set; }
        public Season WorstClassificationSeason { get; set; }

        public int WorstPointsMade { get; set; }
        public Season WorstPointsMadeSeason { get; set; }

        public List<Season> SeasonsPlayed { get; set; }
        public int FixturesPlayed { get; set; }
        public List<GameMatch> GameMatchPlayed { get; set; }

        public int GoalsScored { get { return GoalsScoredHome + GoalsScoredAway; } }
        public int GoalsScoredHome { get; set; }
        public int GoalsScoredAway { get; set; }

        public int GoalsConceded { get { return GoalsConcededHome + GoalsConcededAway; } }
        public int GoalsConcededHome { get; set; }
        public int GoalsConcededAway { get; set; }

        public int Victories
        {
            get { return HomeVictories + AwayVictories; }
        }
        public int HomeVictories { get; set; }
        public int AwayVictories { get; set; }

        public int Draws
        {
            get { return HomeDraws + AwayDraws; }
        }
        public int HomeDraws { get; set; }
        public int AwayDraws { get; set; }

        public int Defeats
        {
            get { return HomeDefeats + AwayDefeats; }
        }
        public int HomeDefeats { get; set; }
        public int AwayDefeats { get; set; }

        public Season Season { get; set; }
        public int SeasonClassification { get; set; }
        public int SeasonPts { get; set; }

    }
}