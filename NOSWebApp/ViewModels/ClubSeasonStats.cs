﻿using NOSWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NOSWebApp.ViewModels
{
    public class ClubSeasonStats
    {
        public int Position { get; set; }
        public Club Club { get; set; }
        public int GamesPlayed { get; set; }
        public int Victories { get; set; }
        public int Draws { get; set; }
        public int Defeats { get; set; }
        public int GoalsScored { get; set; }
        public int GoalsConceded { get; set; }
        public int Pts { get; set; }
    }
}