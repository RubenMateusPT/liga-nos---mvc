﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using NOSWebApp.Models;
using Owin;

[assembly: OwinStartupAttribute(typeof(NOSWebApp.Startup))]
namespace NOSWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRolesAndUsers();
        }

        private void CreateRolesAndUsers()
        {
            using(ApplicationDbContext db = new ApplicationDbContext())
            {                

                //Creation of Default Roles and Users
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));


                #region Roles Creation
                // Creates Role Admin and assing to it's user
                if (!roleManager.RoleExists("Admin"))
                {
                    var role = new IdentityRole();
                    role.Name = "Admin";
                    roleManager.Create(role);

                }

                //Create LeagueAdmin Role
                if (!roleManager.RoleExists("LeagueAdmin"))
                {
                    var role = new IdentityRole();
                    role.Name = "LeagueAdmin";
                    roleManager.Create(role);
                }

                //Create Player Role
                if (!roleManager.RoleExists("Player"))
                {
                    var role = new IdentityRole();
                    role.Name = "Player";
                    roleManager.Create(role);
                } 
                #endregion

                if(userManager.FindByEmail("bettilt@gmail.com") == null)
                {
                    var user = new ApplicationUser();
                    user.UserName = "bettilt";
                    user.Email = "bettilt@gmail.com";
                    user.Birthday = DateTime.Today;
                    user.BankAccount = 100;                    

                    string userPWD = "BetTilt20!9";

                    var chkUser = userManager.Create(user, userPWD);

                    if (chkUser.Succeeded)
                    {
                        userManager.AddToRole(user.Id, "Admin");
                        userManager.AddToRole(user.Id, "LeagueAdmin");
                        userManager.AddToRole(user.Id, "Player");
                    }
                }

                if (userManager.FindByEmail("leagueadmin@gmail.com") == null)
                {
                    var user = new ApplicationUser();
                    user.UserName = "LeagueAdmin";
                    user.Email = "leagueadmin@gmail.com";
                    user.Birthday = DateTime.Today;
                    user.BankAccount = 100;

                    string userPWD = "LeagueAdmin20!9";

                    var chkUser = userManager.Create(user, userPWD);

                    if (chkUser.Succeeded)
                    {
                        userManager.AddToRole(user.Id, "LeagueAdmin");
                        userManager.AddToRole(user.Id, "Player");
                    }
                }
            }

            
        }
    }
}
