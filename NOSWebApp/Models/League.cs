﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NOSWebApp.Models
{
    public class League
    {
        [Key]
        public int LeagueId { get; set; }
        
        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        public string Description { get; set; }

        public string LogoImage { get; set; } = "~/Media/Shared/noImageAvailable.png";

        public bool IsActive { get; set; } = true;

        #region Navigation

        [NotMapped]
        public virtual HttpPostedFileBase ImageUrl { get; set; }

        public virtual ICollection<Club> Clubs { get; set; } 
        
        public virtual ICollection<Season> Seasons { get; set; }

        #endregion

        
    }
}