﻿using NOSWebApp.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NOSWebApp.Models
{
    public enum SeasonStatus
    {
        WaitingToStart,
        InPogress,
        Finished,
        Canceled
    }

    public class Season
    {
        [Key]
        public int SeasonId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int MaxFixtures { get; set; }

        public int MaxGamesMatches { get; set; }

        public SeasonStatus SeasonStatus { get; set; }

        public double SeasonPot { get; set; } = 0;

        public int LeagueId { get; set; }
        public virtual League League { get; set; }

        public virtual ICollection<Club> SeasonClubs { get; set; }

        public virtual ICollection<Fixture> Fixtures { get; set; }

        public virtual ICollection<SeasonBets> Betters { get; set; }


    }

}