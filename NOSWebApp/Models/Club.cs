﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NOSWebApp.Models
{
    public class Club
    {
        [Key]
        public int ClubId { get; set; }

        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        public string Stadium { get; set; }

        public string Description { get; set; }

        public string LogoImage { get; set; } = "~/Media/Shared/noImageAvailable.png";

        public bool IsActive { get; set; } = true;

        public int? LeagueId { get; set; }
        public virtual League League { get; set; }

        [NotMapped]
        public virtual HttpPostedFileBase ImageUrl { get; set; }


        public virtual ICollection<Season> SeasonPlayed { get; set; }

        public virtual ICollection<GameMatch> GameMatches { get; set; }

        public virtual ICollection<ApplicationUser> Fans { get; set; }

        public virtual ICollection<SeasonBets> UserSeasonsBetted { get; set; }
    }
}