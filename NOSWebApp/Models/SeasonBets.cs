﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NOSWebApp.Models
{
    public class SeasonBets
    {
        [Key]
        public int SeasonBetID { get; set; }

        public string UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual ApplicationUser Better { get; set; }

        
        public int SeasonId { get; set; }
        [ForeignKey("SeasonId")]
        public virtual Season SeasonBetted { get; set; }

        public int ClubId { get; set; }
        [ForeignKey("ClubId")]
        public Club ClubBetted { get; set; }

        public double MoneyBetted { get; set; }
    }
}