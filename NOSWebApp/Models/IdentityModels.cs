﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace NOSWebApp.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PostalCode { get; set; }
        public DateTime Birthday { get; set; }

        public double BankAccount { get; set; }
        public virtual ICollection<SeasonBets> SeasonsBetted { get; set; }

        public int? ClubId { get; set; }
        [ForeignKey("ClubId")]
        public Club FavouriteClub { get; set; }

        public string ProfilePic { get; set; } = "~/Media/Shared/noUserImage.png";

        [NotMapped]
        public virtual HttpPostedFileBase ImageURL { get; set; }

        [NotMapped]
        public virtual string FullName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            List<Claim> userClaims = new List<Claim>();
            
            userClaims.Add(new Claim("FullName", this.FullName != null ? this.FullName : ""));            
            userClaims.Add(new Claim("Birthday", this.Birthday.ToShortDateString()));
            userClaims.Add(new Claim("FavouriteClub", this.ClubId.HasValue ? this.ClubId.Value.ToString() : "None"));
            userClaims.Add(new Claim("ProfilePic", this.ProfilePic != null ? this.ProfilePic.Substring(1) : ""));
            userClaims.Add(new Claim("BankAccount", this.BankAccount.ToString()));                        

            userIdentity.AddClaims(userClaims);

            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Migrations.Configuration>());
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();  

            
            
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<NOSWebApp.Models.League> Leagues { get; set; }

        public System.Data.Entity.DbSet<NOSWebApp.Models.Club> Clubs { get; set; }

        public System.Data.Entity.DbSet<NOSWebApp.Models.Season> Seasons { get; set; }

        public DbSet<Fixture> Fixtures { get; set; }

        public DbSet<GameMatch> GameMatches { get; set; }

        public DbSet<SeasonBets> SeasonBets { get; set; }

    }
}