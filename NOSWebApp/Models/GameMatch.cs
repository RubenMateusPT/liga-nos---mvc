﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NOSWebApp.Models
{
    public enum GameMatchStatus
    {
        Win,
        Draw,
        Lose
    }

    public class GameMatch
    {
        [Key]
        public int GameMatchId { get; set; }

        
        public int ClubHomeId { get; set; }
        [ForeignKey("ClubHomeId")]
        public Club HomeClub { get; set; }
        public int HomeGoals { get; set; }
        public GameMatchStatus HomeStatus { get; set; }


        public int ClubAwayId { get; set; }
        [ForeignKey("ClubAwayId")]
        public Club AwayClub { get; set; }
        public int AwayGoals { get; set; }
        public GameMatchStatus AwayStatus { get; set; }


        public int FixtureId { get; set; }
        public virtual Fixture Fixture { get; set; }
    }
}