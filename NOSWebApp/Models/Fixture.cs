﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NOSWebApp.Models
{
    public class Fixture
    {
        [Key]
        public int FixtureId { get; set; }

        public int SeasonId { get; set; }
        public virtual Season Season { get; set; }

        public virtual ICollection<GameMatch> GameMatches { get; set; }
    }
}