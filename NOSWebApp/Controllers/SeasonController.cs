﻿using NOSWebApp.Helpers;
using NOSWebApp.Models;
using NOSWebApp.ViewModels;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using NOSWebApp.Extensions;
using System;
using System.Net;

namespace NOSWebApp.Controllers
{
    [Authorize]
    public class SeasonController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Season
        public ActionResult Index(int? id, string errorMessage)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var season = db.Seasons.FirstOrDefault(s => s.SeasonId == id);

            if(season == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            try
            {
                var seasonStats = StatusHelper.MakeSeasonStatus(season);
                List<ClubSeasonStats> css = seasonStats.ClubSeasonStats.OrderBy(cs => cs.Position).ToList();

                ViewBag.ErrorMessage = errorMessage;
                return View(seasonStats);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult AddFixture(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var season = db.Seasons.FirstOrDefault(s => s.SeasonId == id);

            if(season == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if (!season.League.IsActive)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if((season.SeasonStatus == SeasonStatus.Canceled || season.SeasonStatus == SeasonStatus.Finished))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                var fixture = LeagueHelper.CreateFixture(season);

                if(fixture == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                }

                if (season.SeasonStatus == SeasonStatus.InPogress)
                {
                    var GameMatches = LeagueHelper.CreateGameMatches(season, fixture);

                    if(GameMatches == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                    }

                    fixture.GameMatches = GameMatches;

                    try
                    {
                        db.Fixtures.Add(fixture);

                        db.GameMatches.AddRange(GameMatches);
                    }
                    catch
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                    }

                    if (season.Fixtures.Count == season.MaxFixtures)
                    {
                        try
                        {
                            season.SeasonStatus = SeasonStatus.Finished;
                            season.EndDate = DateTime.Now;
                            db.Entry(season).State = EntityState.Modified;
                        }
                        catch
                        {
                            return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                        }

                        var seasonStats = StatusHelper.MakeSeasonStatus(season);

                        if(seasonStats == null)
                        {
                            return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                        }


                        if (season.SeasonStatus == SeasonStatus.Finished)
                        {
                            List<ClubSeasonStats> css = seasonStats.ClubSeasonStats.OrderBy(cs => cs.Position).ToList();

                            List<SeasonBets> seasonBets = db.SeasonBets.Where(sb => sb.SeasonId == season.SeasonId).ToList();

                            if(css == null || seasonBets == null)
                            {
                                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                            }

                            for (int i = 0; i < css.Count(); i++)
                            {

                                var user = seasonBets.FirstOrDefault(sb => sb.ClubId == css.ElementAt(i).Club.ClubId);

                                if (i != css.Count - 1)
                                {
                                    if (user != null)
                                    {
                                        user.Better.BankAccount += season.SeasonPot * 0.50;
                                    }
                                    season.SeasonPot -= season.SeasonPot * 0.50;


                                }
                                else
                                {
                                    if (user != null)
                                    {
                                        user.Better.BankAccount += season.SeasonPot * 0.50;
                                    }
                                    season.SeasonPot -= season.SeasonPot;
                                }

                                if (user != null)
                                {
                                    try
                                    {
                                        db.Entry(user).State = EntityState.Modified;
                                    }
                                    catch
                                    {
                                        return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                                    }
                                }
                            }

                            db.Entry(season).State = EntityState.Modified;

                            db.SaveChanges();

                        }
                        db.Entry(season).State = EntityState.Modified;

                        db.SaveChanges();
                    }

                    db.SaveChanges();

                    return RedirectToAction("Index", new { id = id });
                }


                season.SeasonStatus = SeasonStatus.Finished;
                season.EndDate = DateTime.Now;
                db.Entry(season).State = EntityState.Modified;
                db.SaveChanges();



                return RedirectToAction("Index", new { id = id });
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }


        public ActionResult CreateSeasonBet(int? id, int? clubId, int? moneyBetted)
        {
            if(id == null || clubId == null || moneyBetted == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var season = db.Seasons.FirstOrDefault(s => s.SeasonId == id);

            if(season == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if(season.SeasonStatus != SeasonStatus.WaitingToStart && !season.League.IsActive)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var betttedClub = season.SeasonClubs.FirstOrDefault(c => c.ClubId == clubId);

            if(betttedClub == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if(moneyBetted == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if(moneyBetted > db.Users.Single( u => u.UserName == User.Identity.Name).BankAccount)
            {
                return RedirectToAction("Index", new { id = id, errorMessage = "You don't have enought balance!" });
            }

            SeasonBets newBet = new SeasonBets()
            {
                SeasonBetted = season,
                ClubBetted = betttedClub,
                Better = db.Users.Single(u => u.UserName == User.Identity.Name),
                MoneyBetted = moneyBetted.Value
            };

            List < SeasonBets > seasonbets = db.SeasonBets.ToList();

            if(seasonbets == null){
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            if (seasonbets.Where(sb => sb.SeasonId == newBet.SeasonBetted.SeasonId && sb.ClubId == newBet.ClubBetted.ClubId).Count() != 0)
            {
                return RedirectToAction("Index", new { id = id, errorMessage = "Sory someone was faster than you" });
            }

            try
            {
                db.SeasonBets.Add(newBet);


                newBet.Better.BankAccount -= moneyBetted.Value;
                db.Entry(newBet.Better).State = EntityState.Modified;

                season.SeasonPot += moneyBetted.Value;
                db.Entry(season).State = EntityState.Modified;

                db.SaveChanges();

                return RedirectToAction("Index", new { id = id });
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
        }

        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult KickUser(string userId,int? seasonId,int? clubId)
        {
            if (userId == null || clubId == null || clubId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var seasonBet = db.SeasonBets.FirstOrDefault(sb => sb.Better.Id.ToUpper() == userId.ToString().ToUpper() && sb.SeasonId == seasonId && sb.ClubId == clubId );

            if(seasonBet == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (!seasonBet.SeasonBetted.League.IsActive)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                seasonBet.SeasonBetted.SeasonPot -= seasonBet.MoneyBetted;
                seasonBet.Better.BankAccount += seasonBet.MoneyBetted;

                db.SeasonBets.Remove(seasonBet);
                db.SaveChanges();

                return RedirectToAction("Index", new { id = seasonId });
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}