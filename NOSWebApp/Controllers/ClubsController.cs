﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NOSWebApp.Helpers;
using NOSWebApp.Models;

namespace NOSWebApp.Controllers
{
    [Authorize]
    [HandleError]
    public class ClubsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Clubs
        [Authorize]
        public ActionResult Index(string errorMessage)
        {
            List<Club> clubs = null;

            try { clubs = db.Clubs.Include(c => c.League).ToList(); }
            catch { return new HttpStatusCodeResult(HttpStatusCode.InternalServerError); }

            if (clubs == null) { return new HttpStatusCodeResult(HttpStatusCode.InternalServerError); }

            ViewBag.ErrorMessage = errorMessage;
            return View(clubs.ToList());
        }


        // GET: Clubs/Create
        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult Create()
        {
            List<League> leagues = null;

            try { leagues = db.Leagues.ToList(); }
            catch { return new HttpStatusCodeResult(HttpStatusCode.InternalServerError); }

            if(leagues ==null) { return new HttpStatusCodeResult(HttpStatusCode.InternalServerError); }

            leagues.Add(new League { Name = "[None]", LeagueId = 0 });
            ViewBag.LeagueId = new SelectList(leagues.OrderBy(l => l.Name), "LeagueId", "Name");


            return View();
        }

        // POST: Clubs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult Create([Bind(Include = "ClubId,Name,Stadium,Description,LogoImage,ImageURL,IsActive,LeagueId")] Club club)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (club.LeagueId == 0)
                    {
                        club.LeagueId = null;
                    }
                    db.Clubs.Add(club);
                    db.SaveChanges();

                    if (club.ImageUrl != null)
                    {
                        var folder = "~/Media/Clubs";
                        var file = $"{club.ClubId}.png";

                        var response = FileHelper.UploadImage(club.ImageUrl, folder, file);

                        if (response)
                        {
                            var pic = $"{folder}/{file}";
                            club.LogoImage = pic;

                            db.Entry(club).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }

                    return RedirectToAction("Index");
                }
            }

            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            List<League> leagues = null;

            try { leagues = db.Leagues.ToList(); }
            catch { return new HttpStatusCodeResult(HttpStatusCode.InternalServerError); }

            if (leagues == null) { return new HttpStatusCodeResult(HttpStatusCode.InternalServerError); }

            leagues.Add(new League { Name = "[None]", LeagueId = 0 });
            ViewBag.LeagueId = new SelectList(leagues.OrderBy(l => l.Name), "LeagueId", "Name");

            return View(club);
        }

        // GET: Clubs/Edit/5
        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Club club = db.Clubs.Find(id);

            if (club == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            List<League> leagues = null;

            try { leagues = db.Leagues.ToList(); }
            catch { return new HttpStatusCodeResult(HttpStatusCode.InternalServerError); }

            if (leagues == null) { return new HttpStatusCodeResult(HttpStatusCode.InternalServerError); }

            leagues.Add(new League { Name = "[None]", LeagueId = 0 });
            
            ViewBag.LeagueId = new SelectList(leagues.OrderBy(l => l.Name), "LeagueId", "Name",club.LeagueId);

            return View(club);
        }

        // POST: Clubs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult Edit([Bind(Include = "ClubId,Name,Stadium,Description,LogoImage,ImageURL,IsActive,LeagueId")] Club club)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (club.LeagueId == 0)
                    {
                        club.LeagueId = null;
                    }

                    if (club.ImageUrl != null)
                    {
                        var folder = "~/Media/Clubs";
                        var file = $"{club.ClubId}.png";

                        var response = FileHelper.UploadImage(club.ImageUrl, folder, file);

                        if (response)
                        {
                            var pic = $"{folder}/{file}";
                            club.LogoImage = pic;
                        }
                    }

                    db.Entry(club).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            List<League> leagues = null;

            try { leagues = db.Leagues.ToList(); }
            catch { return new HttpStatusCodeResult(HttpStatusCode.InternalServerError); }

            if (leagues == null) { return new HttpStatusCodeResult(HttpStatusCode.InternalServerError); }

            leagues.Add(new League { Name = "[None]", LeagueId = 0 });
            ViewBag.LeagueId = new SelectList(leagues.OrderBy(l => l.Name), "LeagueId", "Name", club.LeagueId);

            return View(club);
        }

        // GET: Leagues/Delete/5
        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult Disable(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Club club = db.Clubs.Find(id);

            if (club == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if (club.SeasonPlayed.Where(s => s.SeasonStatus == SeasonStatus.InPogress || s.SeasonStatus == SeasonStatus.WaitingToStart).Count() != 0)
            {

                return RedirectToAction("Index",new { errorMessage = "You can't disable clubs that are playing!" });
            }

            club.IsActive = false;

            db.Entry(club).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult Enable(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Club club = db.Clubs.Find(id);
            if (club == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            club.IsActive = true;

            

            try
            {
                db.Entry(club);
                db.SaveChanges();
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
