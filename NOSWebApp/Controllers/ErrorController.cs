﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NOSWebApp.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NotFound()
        {
            Response.StatusCode = 404;

            return View();
        }

        public ActionResult BadRequest()
        {
            Response.StatusCode = 400;

            return View();
        }

        public ActionResult InternalError()
        {
            Response.StatusCode = 500;

            return View();
        }
    }
}