﻿using Microsoft.AspNet.Identity;
using NOSWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace NOSWebApp.Controllers
{
    [Authorize]
    [HandleError]
    public class DashboardController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Dashboard
        [Authorize]
        public ActionResult Home(string id)
        {
            
            var users = db.Users;

            ApplicationUser user = null;

            if (id == null)
            {
                try
                {
                    var thisUserId = User.Identity.GetUserId().ToUpper();

                    user = users.FirstOrDefault(u => u.Id.ToUpper() == thisUserId);

                    user.FavouriteClub = db.Clubs.FirstOrDefault(c => c.ClubId == user.ClubId);

                    return View(user);
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                }
            }

            try
            {
                user = users.FirstOrDefault(u => u.Id.ToUpper() == id.ToUpper());

                user.FavouriteClub = db.Clubs.FirstOrDefault(c => c.ClubId == user.ClubId);


                return View(user);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        [Authorize]
        public ActionResult Bank()
        {

            return View();
        }

        [Authorize]
        public ActionResult AddMoney(string userId, double moneyToAdd)
        {
            if(userId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if(moneyToAdd == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                var user = db.Users.FirstOrDefault(u => u.Id.ToUpper() == userId.ToUpper());

                user.BankAccount += moneyToAdd;

                db.Entry(user).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();

                return RedirectToAction("Bank");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        [Authorize]
        public ActionResult About()
        {
            return View();
        }

        [Authorize]
        public ActionResult Contact()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}