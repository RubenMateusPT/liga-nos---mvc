﻿using NOSWebApp.Helpers;
using NOSWebApp.Models;
using NOSWebApp.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace NOSWebApp.Controllers
{
    [Authorize]
    public class LeagueController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: League
        [Authorize]
        public ActionResult Index(int? id, string errorMessage)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            try
            {
                var league = db.Leagues.FirstOrDefault(l => l.LeagueId == id);

                if(league == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var leagueStatsView = StatusHelper.MakeLeagueStats(league);

                ViewBag.ErrorMessage = errorMessage;
                return View(leagueStatsView);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

        }

        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult AddSeason(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var league = db.Leagues.FirstOrDefault(l => l.LeagueId == id);

            if(league == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if (!league.IsActive)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (league != null && league.IsActive)
            {
                if (league.Clubs.Where(c => c.IsActive).Count() >= 4)
                {
                    try
                    {
                        var newSeason = LeagueHelper.CreateSeason(league);

                        if(newSeason == null)
                        {
                            return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                        }

                        league.Seasons.Add(newSeason);

                        db.Seasons.Add(newSeason);
                        db.Entry(league).State = EntityState.Modified;

                        db.SaveChanges();

                        return RedirectToAction("Index", new { id = id });
                    }
                    catch
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                    }
                }

                    return RedirectToAction("Index", new { id = league.LeagueId, errorMessage = "Not enough active clubs to start season!" });

            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult CancelSeason(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var season = db.Seasons.FirstOrDefault(s => s.SeasonId == id);

            if(season == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if(season.Betters.Count() != 0)
            {
                return RedirectToAction("Index", new { id = season.LeagueId, errorMessage = "You can't disable a season that has betters!" });
            }

            if (season.SeasonStatus != SeasonStatus.WaitingToStart)
            {
                return RedirectToAction("Index", new { id = season.LeagueId, errorMessage = "You can only disable seasons that are about to start!" });
            }


            if (!season.League.IsActive)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                season.SeasonStatus = SeasonStatus.Canceled;

                db.Entry(season).State = EntityState.Modified;

                db.SaveChanges();

                return RedirectToAction("Index", new { id = season.LeagueId });
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}