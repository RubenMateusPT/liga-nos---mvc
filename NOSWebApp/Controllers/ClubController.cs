﻿using NOSWebApp.Helpers;
using NOSWebApp.Models;
using NOSWebApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace NOSWebApp.Controllers
{
    [Authorize]
    [HandleError]
    public class ClubController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Club
        [Authorize]
        public ActionResult Index(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "No Club Given");
            }

            Club club = null;
            try
            {
                 club = db.Clubs.FirstOrDefault(c => c.ClubId == id);

            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message);
            }

            if(club == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound, "Club not Found");
            }

            ClubStatsView clubStatsView = new ClubStatsView
            {
                Club = club,
                SeasonsPlayed = club.SeasonPlayed.ToList(),
            };

            return View(clubStatsView);
        }

        [Authorize]
        public ActionResult AllStats(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "No Club Given");
            }

            Club club = null;
            try
            {
                club = db.Clubs.FirstOrDefault(c => c.ClubId == id);
            }
            catch(Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message);
            }

            if (club == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound, "Club not Found");
            }

            List<GameMatch> clubGameMatches = null;

            try
            {
                clubGameMatches = db.GameMatches.Where(gm => gm.HomeClub.ClubId == club.ClubId || gm.AwayClub.ClubId == club.ClubId).ToList();
            }
            catch(Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message);
            }

            if(clubGameMatches == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            ClubStatsView clubStatsView = null;

            try
            {
                clubStatsView = StatusHelper.MakeClubStats(club, clubGameMatches);
            }
            catch(Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message);
            }

            if(clubStatsView == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            return View(clubStatsView);
        }

        [Authorize]
        public ActionResult AllSeasonClubStats(int? id, int? seasonId)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "No Club Given");
            }

            Club club = null;
            try
            {
                club = db.Clubs.FirstOrDefault(c => c.ClubId == id);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message);
            }

            if (club == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound, "Club not Found");
            }

            if (seasonId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "No Season Given");
            }

            List<GameMatch> clubGameMatches = null;

            try
            {
                clubGameMatches = db.GameMatches.Where(gm => (gm.HomeClub.ClubId == club.ClubId || gm.AwayClub.ClubId == club.ClubId) && gm.Fixture.SeasonId == seasonId).ToList();
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            if(clubGameMatches == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            ClubStatsView clubStatsView = null;

            try
            {
                clubStatsView = StatusHelper.MakeClubSeasonStats(club, clubGameMatches);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            if(clubStatsView == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            return View(clubStatsView);
        }

        [Authorize]
        public ActionResult ClubFans(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Club club = null;
            try
            {
             club =   db.Clubs.FirstOrDefault(c => c.ClubId == id);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            if(club == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            return View(club);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}