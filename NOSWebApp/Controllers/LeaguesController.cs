﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NOSWebApp.Helpers;
using NOSWebApp.Models;

namespace NOSWebApp.Controllers
{
    [Authorize]
    public class LeaguesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Leagues
        [Authorize]
        public ActionResult Index(string errorMessage)
        {
            var leagues = db.Leagues.OrderBy(l => l.Name).ToList();

            if(leagues == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            ViewBag.ErrorMessage = errorMessage;
            return View(leagues);

        }                

        [Authorize(Roles = "LeagueAdmin")]
        // GET: Leagues/Create
        public ActionResult Create()
        {
            return View(new League());
        }

        // POST: Leagues/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult Create(League league)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Leagues.Add(league);
                    db.SaveChanges();

                    if (league.ImageUrl != null)
                    {
                        var folder = "~/Media/Leagues";
                        var file = $"{league.LeagueId}.png";

                        var response = FileHelper.UploadImage(league.ImageUrl, folder, file);

                        if (response)
                        {
                            var pic = $"{folder}/{file}";
                            league.LogoImage = pic;

                            db.Entry(league).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }

                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                }
            }

            return View(league);
        }

        // GET: Leagues/Edit/5
        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            League league = db.Leagues.Find(id);
            

            if (league == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            return View(league);
        }

        // POST: Leagues/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult Edit([Bind(Include = "LeagueId,Name,Description,LogoImage,ImageUrl")]League league)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (league.ImageUrl != null)
                    {
                        var folder = "~/Media/Leagues";
                        var file = $"{league.LeagueId}.png";

                        var response = FileHelper.UploadImage(league.ImageUrl, folder, file);

                        if (response)
                        {
                            var pic = $"{folder}/{file}";
                            league.LogoImage = pic;
                        }
                    }

                    db.Entry(league).State = EntityState.Modified;
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                catch
                {
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                }
            }

            return View(league);
        }

        // GET: Leagues/Delete/5
        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult Disable(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            League league = db.Leagues.Find(id);
            if (league == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if(league.Seasons.Where(s => s.SeasonStatus == SeasonStatus.WaitingToStart || s.SeasonStatus == SeasonStatus.InPogress).Count() != 0)
            {
                return RedirectToAction("Index", new { errorMessage = "You can't disable league that have pendant/ on going seasons!" });
            }

            if (!league.IsActive)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                league.IsActive = false;

                db.Entry(league).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        [Authorize(Roles = "LeagueAdmin")]
        public ActionResult Enable(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            League league = db.Leagues.Find(id);

            if(league == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if (league.IsActive)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                league.IsActive = true;

                db.Entry(league);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
