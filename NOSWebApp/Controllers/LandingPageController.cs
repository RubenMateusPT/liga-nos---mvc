﻿using NOSWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NOSWebApp.Controllers
{
    public class LandingPageController : Controller
    {
        // GET: LandingPage
        [AllowAnonymous]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Home", "Dashboard");
            }

            return View();
        }

    }
}