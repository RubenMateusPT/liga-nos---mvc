﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NOSWebApp.Helpers;
using NOSWebApp.Models;
using NOSWebApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace NOSWebApp.Controllers
{
    public class UsersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Users
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {            

            var usersView = new List<UserView>();

            if(usersView == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            try
            {
                foreach (var user in db.Users.OrderBy(u => u.UserName).ToList())
                {
                    var userView = new UserView
                    {
                        ProfilePic = user.ProfilePic,
                        UserID = user.Id,
                        Username = user.UserName,
                        Email = user.Email,
                        FullName = user.FullName,
                        Birthday = user.Birthday,
                        Roles = new List<RoleView>()
                    };

                    foreach (var role in db.Roles.OrderBy(r => r.Name).ToList())
                    {
                        foreach (var userRole in user.Roles)
                        {
                            if (role.Id == userRole.RoleId)
                            {
                                var roleView = new RoleView
                                {
                                    RoleID = role.Id,
                                    Name = role.Name
                                };

                                userView.Roles.Add(roleView);
                            }
                        }
                    }

                    usersView.Add(userView);
                }

                return View(usersView);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
            
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Roles(string userID)
        {
            if (string.IsNullOrEmpty(userID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var roles = roleManager.Roles.ToList();
            var users = userManager.Users.ToList();
            var user = users.Find(u => u.Id == userID);

            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            var rolesView = new List<RoleView>();
            foreach (var item in user.Roles)
            {
                var role = roles.Find(r => r.Id == item.RoleId);
                var roleView = new RoleView
                {
                    RoleID = role.Id,
                    Name = role.Name,
                };

                rolesView.Add(roleView);
            }

            var userView = new UserView
            {
                Email = user.Email,
                Username = user.UserName,
                UserID = user.Id,
                Roles = rolesView,
            };

            return View(userView);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AddRole(string userID)
        {
            if (string.IsNullOrEmpty(userID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var users = userManager.Users.ToList();
            var user = users.Find(u => u.Id == userID);

            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            var userView = new UserView
            {
                Email = user.Email,
                Username = user.UserName,
                UserID = user.Id,
            };


            ViewBag.RoleID = new SelectList(db.Roles.ToList(), "Id", "Name");

            return View(userView);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult AddRole(string userID, FormCollection form)
        {

            var roleID = Request["RoleID"];



            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var users = userManager.Users.ToList();
            var user = users.Find(u => u.Id == userID);

            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            var userView = new UserView
            {
                Email = user.Email,
                Username = user.UserName,
                UserID = user.Id,
            };

            if (string.IsNullOrEmpty(roleID))
            {
                ViewBag.Error = "Tem que selecionar uma permissão";

                ViewBag.RoleID = new SelectList(db.Roles.ToList(), "Id", "Name");

                return View(userView);
            }

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var roles = roleManager.Roles.ToList();
            var role = roles.Find(r => r.Id == roleID);

            if(role == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if (!userManager.IsInRole(userID, role.Name))
            {
                userManager.AddToRole(userID, role.Name);
            }

            var rolesView = new List<RoleView>();

            foreach (var item in user.Roles)
            {
                role = roles.Find(r => r.Id == item.RoleId);
                var roleView = new RoleView
                {
                    Name = role.Name,
                    RoleID = role.Id,
                };

                rolesView.Add(roleView);
            }

            userView = new UserView
            {
                Email = user.Email,
                Username = user.UserName,
                Roles = rolesView,
                UserID = user.Id,
            };

            return View("Roles", userView);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(string userID, string roleID)
        {
            if (string.IsNullOrEmpty(userID) || string.IsNullOrEmpty(roleID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var user = userManager.Users.ToList().Find(u => u.Id == userID);
            var role = roleManager.Roles.ToList().Find(r => r.Id == roleID);

            if (userManager.IsInRole(user.Id, role.Name))
            {
                userManager.RemoveFromRole(user.Id, role.Name);
            }

            if(user == null || role == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            var users = userManager.Users.ToList();
            var roles = roleManager.Roles.ToList();
            var rolesView = new List<RoleView>();

            foreach (var item in user.Roles)
            {
                role = roles.Find(r => r.Id == item.RoleId);
                var roleView = new RoleView
                {
                    Name = role.Name,
                    RoleID = role.Id,
                };

                rolesView.Add(roleView);
            }

            var userView = new UserView
            {
                Email = user.Email,
                Username = user.UserName,
                Roles = rolesView,
                UserID = user.Id,
            };

            return View("Roles", userView);


        }

        [Authorize]
        public ActionResult EditUser(string userId)
        {
            if (userId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ApplicationUser user = db.Users.FirstOrDefault(u => u.Id.ToUpper() == userId.ToUpper());

            if(user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if (user.Id.ToUpper() != User.Identity.GetUserId().ToUpper())
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            List<Club> clubs = db.Clubs.ToList();
            clubs.Add(new Club { Name = "[None]", LeagueId = 0 });
            ViewBag.ClubId = new SelectList(clubs.OrderBy(l => l.Name), "ClubId", "Name", user.ClubId);

            return View(user);
        }


        [HttpPost]
        [Authorize]
        public ActionResult EditUser([Bind(Include = "Id,Birthday,BankAccount,PostalCode,ProfilePic,UserName,Email,FirstName,LastName,ImageURL,ClubId")]ApplicationUser user)
        {
            if(user.Id.ToUpper() != User.Identity.GetUserId().ToUpper())
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                if (ModelState.IsValid)
                {

                    db.Users.Attach(user);


                    if (user.ClubId == 0)
                    {
                        user.ClubId = null;
                    }

                    if (user.ImageURL != null)
                    {
                        var folder = "~/Media/Users";
                        var file = $"{user.Id}.png";

                        var response = FileHelper.UploadImage(user.ImageURL, folder, file);

                        if (response)
                        {
                            var pic = $"{folder}/{file}";
                            user.ProfilePic = pic;
                        }
                    }


                    db.Entry(user).Property("FirstName").IsModified = true;
                    db.Entry(user).Property("LastName").IsModified = true;
                    db.Entry(user).Property("ClubId").IsModified = true;
                    db.Entry(user).Property("ProfilePic").IsModified = true;

                    db.SaveChanges();
                    return RedirectToAction("Home", "Dashboard");
                }

                List<Club> clubs = db.Clubs.ToList();
                clubs.Add(new Club { Name = "[None]", LeagueId = 0 });
                ViewBag.ClubId = new SelectList(clubs.OrderBy(l => l.Name), "ClubId", "Name", user.ClubId);
                return View(user);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
}
}