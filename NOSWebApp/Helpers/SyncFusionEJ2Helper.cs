﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NOSWebApp.Helpers
{
    public class SyncFusionEJ2Helper
    {
    }

    public class PieWithLegendChartData
    {
        public string xValue { get; set; }
        public double yValue { get; set; }
        public string text { get; set; }
    }
}