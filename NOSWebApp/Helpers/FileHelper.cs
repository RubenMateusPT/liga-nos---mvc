﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace NOSWebApp.Helpers
{
    public class FileHelper
    {
        public static bool UploadImage(HttpPostedFileBase file, string folder, string name)
        {
            if (file == null || string.IsNullOrEmpty(folder) || string.IsNullOrEmpty(name))
            {
                return false;
            }

            try
            {
                string path = string.Empty;

                if (file != null)
                {
                    string folderPath = Path.Combine(HttpContext.Current.Server.MapPath(folder));

                    path = Path.Combine(HttpContext.Current.Server.MapPath(folder), name);

                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }

                    file.SaveAs(path);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        file.InputStream.CopyTo(ms);

                        byte[] array = ms.GetBuffer();
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}