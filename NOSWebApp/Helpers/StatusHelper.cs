﻿using NOSWebApp.Models;
using NOSWebApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NOSWebApp.Helpers
{
    public class StatusHelper
    {
        internal static SeasonStatsView MakeSeasonStatus(Season season)
        {
            SeasonStatsView seasonStatsView = new SeasonStatsView
            {
                Season = season,

                TotalFixtures = season.Fixtures.Count,
                TotalGameMatches = season.Fixtures.Sum(f => f.GameMatches.Count()),
                TotalGoalsScored = season.Fixtures.Sum(f => f.GameMatches.Sum(gm => gm.HomeGoals + gm.AwayGoals)),

                ClubSeasonStats = new List<ClubSeasonStats>(),

            };

            foreach (Club club in season.SeasonClubs)
            {
                List<GameMatch> clubGamesMatches = new List<GameMatch>();

                foreach (Fixture fixture in season.Fixtures)
                {
                    clubGamesMatches.Add(fixture.GameMatches.Where(gm => gm.HomeClub == club || gm.AwayClub == club).ToList().First());
                }

                seasonStatsView.ClubSeasonStats.Add(new ClubSeasonStats
                {

                    Club = club,

                    GamesPlayed = clubGamesMatches.Where(gm => gm.Fixture.SeasonId == season.SeasonId).Count(),

                    Victories = clubGamesMatches.Where(gm => gm.HomeClub == club || gm.AwayClub == club).Sum(gm =>
                    {
                        if (gm.HomeClub == club && gm.HomeStatus == GameMatchStatus.Win)
                        {
                            return 1;
                        }
                        if (gm.AwayClub == club && gm.AwayStatus == GameMatchStatus.Win)
                        {
                            return 1;
                        }

                        return 0;
                    }),

                    Draws = clubGamesMatches.Where(gm => gm.HomeClub == club || gm.AwayClub == club).Sum(gm =>
                    {
                        if (gm.HomeClub == club && gm.HomeStatus == GameMatchStatus.Draw)
                        {
                            return 1;
                        }
                        if (gm.AwayClub == club && gm.AwayStatus == GameMatchStatus.Draw)
                        {
                            return 1;
                        }

                        return 0;
                    }),

                    Defeats = clubGamesMatches.Where(gm => gm.HomeClub == club || gm.AwayClub == club).Sum(gm =>
                    {
                        if (gm.HomeClub == club && gm.HomeStatus == GameMatchStatus.Lose)
                        {
                            return 1;
                        }
                        if (gm.AwayClub == club && gm.AwayStatus == GameMatchStatus.Lose)
                        {
                            return 1;
                        }

                        return 0;
                    }),

                    GoalsScored = clubGamesMatches.Where(gm => gm.HomeClub == club || gm.AwayClub == club).Sum(gm =>
                    {
                        if (gm.HomeClub == club)
                        {
                            return gm.HomeGoals;
                        }
                        if (gm.AwayClub == club)
                        {
                            return gm.AwayGoals;
                        }

                        return 0;
                    }),

                    GoalsConceded = clubGamesMatches.Where(gm => gm.HomeClub == club || gm.AwayClub == club).Sum(gm =>
                    {
                        if (gm.HomeClub == club)
                        {
                            return gm.AwayGoals;
                        }
                        if (gm.AwayClub == club)
                        {
                            return gm.HomeGoals;
                        }

                        return 0;
                    }),

                });
            }

            foreach (ClubSeasonStats stat in seasonStatsView.ClubSeasonStats)
            {
                stat.Pts = (stat.Victories * 3) + (stat.Draws * 1);
            }

            seasonStatsView.ClubSeasonStats = seasonStatsView.ClubSeasonStats.OrderByDescending(css => css.Pts).ThenByDescending(css => css.GoalsScored).ThenByDescending(css => css.GoalsConceded).ToList();

            int position = 1;
            foreach (ClubSeasonStats stat in seasonStatsView.ClubSeasonStats)
            {
                stat.Position = position;
                position++;
            }

            return seasonStatsView;

        }

        internal static LeagueStatsView MakeLeagueStats(League league)
        {
            if (league.Seasons.Count != 0)
            {
                LeagueStatsView leagueStatsView = new LeagueStatsView
                {
                    League = league,

                    TotalSeason = league.Seasons.Count(),
                    TotalFixtures = league.Seasons.Sum(s => s.Fixtures.Count()),
                    TotalGameMatches = league.Seasons.Sum(s => s.Fixtures.Sum(f => f.GameMatches.Count())),
                    TotalGoalsScored = league.Seasons.Sum(s => s.Fixtures.Sum(f => f.GameMatches.Sum(gm => gm.AwayGoals + gm.HomeGoals))),


                };

                int mostPoints = 0;
                Club clubVictories = null;

                int leastPoints = 0;
                Club clubDefeats = null;

                int mostGoals = 0;
                Club clubGoals = null;

                int mostConceded = 0;
                Club clubconceded = null;

                foreach (Club club in league.Clubs)
                {
                    int pts = 0;
                    int goals = 0;
                    int conceded = 0;

                    if (club.IsActive)
                    {

                        foreach (Season season in league.Seasons.Where(s => s.SeasonStatus == SeasonStatus.Finished))
                        {
                            foreach (Fixture fixture in season.Fixtures)
                            {
                                foreach (GameMatch gameMatch in fixture.GameMatches)
                                {
                                    if (club == gameMatch.HomeClub)
                                    {
                                        goals += gameMatch.HomeGoals;
                                        conceded += gameMatch.AwayGoals;

                                        if (gameMatch.HomeStatus == GameMatchStatus.Win)
                                        {
                                            pts += 3;
                                        }

                                        if (gameMatch.HomeStatus == GameMatchStatus.Draw)
                                        {
                                            pts += 1;
                                        }

                                        //pts += gameMatch.HomeGoals;
                                        //pts -= gameMatch.AwayGoals;

                                    }
                                    else if (club == gameMatch.AwayClub)
                                    {
                                        goals += gameMatch.AwayGoals;
                                        conceded += gameMatch.HomeGoals;

                                        if (gameMatch.AwayStatus == GameMatchStatus.Win)
                                        {
                                            pts += 3;
                                        }

                                        if (gameMatch.AwayStatus == GameMatchStatus.Draw)
                                        {
                                            pts += 1;
                                        }

                                        pts -= gameMatch.HomeGoals;
                                        pts += gameMatch.AwayGoals;

                                    }
                                }
                            }


                        }



                        if (mostPoints <= pts)
                        {
                            if (leastPoints == 0)
                            {
                                leastPoints = pts;
                            }

                            mostPoints = pts;
                            clubVictories = club;
                        }

                        if (pts <= leastPoints)
                        {
                            leastPoints = pts;
                            clubDefeats = club;
                        }

                        if (mostGoals <= goals)
                        {
                            if (mostConceded == 0)
                            {
                                mostConceded = conceded;
                            }

                            mostGoals = goals;
                            clubGoals = club;
                        }

                        if (conceded >= mostConceded)
                        {
                            mostConceded = conceded;
                            clubconceded = club;
                        }
                    }
                }

                leagueStatsView.ClubMostVictories = clubVictories;
                leagueStatsView.ClubMostDefeats = clubDefeats;
                leagueStatsView.ClubMostGoalsScored = clubGoals;
                leagueStatsView.ClubMostGoalsConceded = clubconceded;

                return leagueStatsView;
            }

            return new LeagueStatsView()
            {
                League = league,
                TotalFixtures = 0,
                TotalGameMatches = 0,
                TotalGoalsScored = 0,
                TotalSeason = 0,
                
        };
        }

        internal static ClubStatsView MakeClubStats(Club club, List<GameMatch> clubGameMatches)
        {
            ClubStatsView clubStatsView = new ClubStatsView
            {
                Club = club,

                SeasonsPlayed = club.SeasonPlayed.ToList(),
                FixturesPlayed = club.SeasonPlayed.Sum(s => s.Fixtures.Count),
                GameMatchPlayed = clubGameMatches,

            };


            foreach (GameMatch gameMatch in clubStatsView.GameMatchPlayed.Where(gm => gm.Fixture.Season.SeasonStatus == SeasonStatus.Finished))
            {
                if (gameMatch.HomeClub == club)
                {
                    clubStatsView.GoalsScoredHome += gameMatch.HomeGoals;
                    clubStatsView.GoalsConcededHome += gameMatch.AwayGoals;

                    if (gameMatch.HomeStatus == GameMatchStatus.Win)
                    {

                        clubStatsView.HomeVictories++;

                    }
                    else if (gameMatch.HomeStatus == GameMatchStatus.Draw)
                    {
                        clubStatsView.HomeDraws++;
                    }
                    else
                    {
                        clubStatsView.HomeDefeats++;
                    }


                }
                else if (gameMatch.AwayClub == club)
                {
                    clubStatsView.GoalsScoredAway += gameMatch.AwayGoals;
                    clubStatsView.GoalsConcededAway += gameMatch.HomeGoals;

                    if (gameMatch.AwayStatus == GameMatchStatus.Win)
                    {

                        clubStatsView.AwayVictories++;

                    }
                    else if (gameMatch.AwayStatus == GameMatchStatus.Draw)
                    {
                        clubStatsView.AwayDraws++;
                    }
                    else
                    {
                        clubStatsView.AwayDefeats++;
                    }
                }
            }

            List<SeasonStatsView> seasonStatsViews = new List<SeasonStatsView>();
            foreach (Season season in club.League.Seasons.Where(s => s.SeasonStatus == SeasonStatus.Finished))
            {
                seasonStatsViews.Add(StatusHelper.MakeSeasonStatus(season));
            }


            int maxPts = 0;
            int minPts = 0;
            int maxPos = 0;
            int minPos = 0;
            foreach (SeasonStatsView seasonStatsView in seasonStatsViews)
            {
                int pts = 0;
                int classification = 0;

                foreach (ClubSeasonStats clubSeasonStats in seasonStatsView.ClubSeasonStats)
                {
                    if (clubSeasonStats.Club == club)
                    {

                        pts = clubSeasonStats.Pts;
                        classification = clubSeasonStats.Position;
                    }
                }

                if (maxPts <= pts)
                {
                    if (minPts == 0)
                    {
                        minPts = pts;
                        maxPos = classification;
                    }

                    maxPts = pts;

                    clubStatsView.BestPointsMadeSeason = seasonStatsView.Season;
                }

                if (classification <= maxPos)
                {
                    if (minPos == 0)
                    {
                        minPos = classification;
                    }

                    maxPos = classification;

                    clubStatsView.BestClassificationSeason = seasonStatsView.Season;
                }

                if (pts <= minPts)
                {
                    minPts = pts;

                    clubStatsView.WorstPointsMadeSeason = seasonStatsView.Season;
                }

                if (classification >= minPos)
                {
                    minPos = classification;

                    clubStatsView.WorstClassificationSeason = seasonStatsView.Season;
                }

            }


            clubStatsView.BestPoints = maxPts;
            clubStatsView.BestClassification = maxPos;

            clubStatsView.WorstPointsMade = minPts;
            clubStatsView.WorstClassification = minPos;

            return clubStatsView;
        }

        internal static ClubStatsView MakeClubSeasonStats(Club club, List<GameMatch> clubGameMatches)
        {
            ClubStatsView clubStatsView = new ClubStatsView
            {
                Club = club,

                Season = clubGameMatches.First().Fixture.Season,                
                FixturesPlayed = club.SeasonPlayed.FirstOrDefault(s => s.SeasonId == clubGameMatches.First().Fixture.SeasonId).Fixtures.Count(),
                GameMatchPlayed = clubGameMatches,

            };


            foreach (GameMatch gameMatch in clubStatsView.GameMatchPlayed.Where(gm => gm.Fixture.Season.SeasonStatus == SeasonStatus.Finished))
            {
                if (gameMatch.HomeClub == club)
                {
                    clubStatsView.GoalsScoredHome += gameMatch.HomeGoals;
                    clubStatsView.GoalsConcededHome += gameMatch.AwayGoals;

                    if (gameMatch.HomeStatus == GameMatchStatus.Win)
                    {

                        clubStatsView.HomeVictories++;

                    }
                    else if (gameMatch.HomeStatus == GameMatchStatus.Draw)
                    {
                        clubStatsView.HomeDraws++;
                    }
                    else
                    {
                        clubStatsView.HomeDefeats++;
                    }


                }
                else if (gameMatch.AwayClub == club)
                {
                    clubStatsView.GoalsScoredAway += gameMatch.AwayGoals;
                    clubStatsView.GoalsConcededAway += gameMatch.HomeGoals;

                    if (gameMatch.AwayStatus == GameMatchStatus.Win)
                    {

                        clubStatsView.AwayVictories++;

                    }
                    else if (gameMatch.AwayStatus == GameMatchStatus.Draw)
                    {
                        clubStatsView.AwayDraws++;
                    }
                    else
                    {
                        clubStatsView.AwayDefeats++;
                    }
                }
            }

            ClubSeasonStats seasonStatsView = StatusHelper.MakeSeasonStatus(club.League.Seasons.FirstOrDefault(s => s.SeasonId == clubGameMatches.First().Fixture.SeasonId)).ClubSeasonStats.FirstOrDefault(ssv => ssv.Club.ClubId == club.ClubId);

            clubStatsView.SeasonClassification = seasonStatsView.Position;
            clubStatsView.SeasonPts = seasonStatsView.Pts;

            return clubStatsView;
        }
    }
}