﻿using NOSWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace NOSWebApp.Helpers
{
    internal class LeagueHelper
    {

        #region SeasonHelper
        internal static Season CreateSeason(League currentLeague)
        {
            var seasonClubs = ChooseSeasonTeam(currentLeague);
            int maxFixtures = FindMaxFixtures(seasonClubs);
            int maxGameMatches = FindMaxGameMatches(seasonClubs);

            Season newSeason = new Season
            {
                League = currentLeague,
                SeasonClubs = ChooseSeasonTeam(currentLeague),
                MaxFixtures = maxFixtures,
                MaxGamesMatches = maxGameMatches,
                StartDate = DateTime.Now
            };

            return newSeason;


        }
        private static List<Club> ChooseSeasonTeam(League currentLeague)
        {
            List<Club> SeasonClubs = new List<Club>();
            int SeasonMaxClubs = currentLeague.Clubs.Count;

            while (SeasonMaxClubs % 4 != 0)
            {
                SeasonMaxClubs--;
            }

            for (int i = 0; i < SeasonMaxClubs; i++)
            {
                Club clubToAdd;
                do
                {
                    Random rdn = new Random();
                    int choise = rdn.Next(0, currentLeague.Clubs.Count);

                    Thread.Sleep(100);

                    clubToAdd = currentLeague.Clubs.ElementAt(choise);

                } while (SeasonClubs.Contains(clubToAdd) || !clubToAdd.IsActive);

                SeasonClubs.Add(clubToAdd);
            }

            return SeasonClubs;
        }

        private static int FindMaxFixtures(List<Club> seasonClubs)
        {
            return ((seasonClubs.Count - 1) * 2);
        }

        private static int FindMaxGameMatches(List<Club> seasonClubs)
        {
            int n = 1;
            const int p = 2;
            int np = 1;

            for (int i = seasonClubs.Count; i > 0; i--)
            {
                n *= i;
            }

            for (int i = seasonClubs.Count - p; i > 0; i--)
            {
                np *= i;
            }

            return ((n / (p * np)) * 2);
        }

        

        #endregion

        internal static Fixture CreateFixture(Season currentSeason)
        {
            if (currentSeason.Fixtures.Count != currentSeason.MaxFixtures)
            {
                currentSeason.SeasonStatus = SeasonStatus.InPogress;

                return new Fixture() {Season = currentSeason };
            }
            else
            {
                currentSeason.SeasonStatus = SeasonStatus.Finished;
                currentSeason.EndDate = DateTime.Now;
            }
           
            return null;
        }

        internal static List<GameMatch> CreateGameMatches(Season currentSeason, Fixture currentFixture)
        {
            List<GameMatch> gameMatches = new List<GameMatch>();

            while (gameMatches.Count != currentSeason.MaxGamesMatches / currentSeason.MaxFixtures)
            {
                foreach (Club homeClub in currentSeason.SeasonClubs)
                {
                    foreach (Club awayClub in currentSeason.SeasonClubs)
                    {
                        if(homeClub != awayClub && !ClubPlayedThisFixture(gameMatches,homeClub,awayClub) && !ClubPlayedThisSeason(currentSeason, homeClub, awayClub))
                        {
                             gameMatches.Add(NewGameMatch(currentFixture, homeClub, awayClub));
                        }
                    }
                }
            }

            return gameMatches;
        }        

        private static bool ClubPlayedThisSeason(Season currentSeason, Club homeClub, Club awayClub)
        {
            foreach(Fixture fixture in currentSeason.Fixtures)
            {
                foreach(GameMatch match in fixture.GameMatches)
                {
                    if (match.HomeClub == homeClub && match.AwayClub == awayClub)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static bool ClubPlayedThisFixture(List<GameMatch> gameMatches, Club homeClub, Club awayClub)
        {
            foreach (GameMatch match in gameMatches)
            {
                if (match.HomeClub == homeClub || match.AwayClub == homeClub || match.AwayClub == awayClub || match.HomeClub == awayClub)
                {
                    return true;
                }
            }
            return false;
        }

        private static GameMatch NewGameMatch(Fixture currentFixture, Club homeClub, Club awayClub)
        {
            Random rdn = new Random();
            int homeGoal = rdn.Next(0,6);
            Thread.Sleep(100);
            int awayGoal = rdn.Next(0,6);

            GameMatchStatus homeStat = GameMatchStatus.Draw;
            GameMatchStatus awayStat = GameMatchStatus.Draw;

            if (homeGoal > awayGoal)
            {
                homeStat = GameMatchStatus.Win;
                awayStat = GameMatchStatus.Lose;
            }
            else if(homeGoal < awayGoal){
                homeStat = GameMatchStatus.Lose;
                awayStat = GameMatchStatus.Win;
            }

            return new GameMatch
            {
                HomeClub = homeClub,
                HomeGoals = homeGoal,
                HomeStatus = homeStat,

                AwayClub = awayClub,
                AwayGoals = awayGoal,
                AwayStatus = awayStat,

                Fixture = currentFixture,
                
                
            };
        }
    }
}