﻿using BundleTransformer.Core.Bundles;
using System.Web;
using System.Web.Optimization;

namespace NOSWebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/syncfusionEJ2").Include(
                       "~/Scripts/ej2/ej2.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      //       "~/Content/bootstrap.css",                      
                      "~/Content/site.css"));

            bundles.Add(new CustomStyleBundle("~/Content/scss").Include(
                "~/Content/bootstrap/bootstrap.scss",
                "~/Content/ej2/bootstrap4.css",
                "~/Content/Style.scss"));

            bundles.Add(new CustomStyleBundle("~/Content/Alternativescss").Include(
                "~/Content/bootstrap/bootstrap.scss",
                "~/Content/ej2/bootstrap4.css",
                "~/Content/LandingPage.scss"));
        }
    }
}
