﻿using Microsoft.AspNet.Identity;
using NOSWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace NOSWebApp.Extensions
{
    public static class IdentityExtension
    {

        public static string GetLastUserName(this IIdentity identity)
        {
            var user = new ApplicationUser();

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var users = db.Users.ToList();
                user = users.FirstOrDefault(u => u.Id.ToUpper() == identity.GetUserId().ToUpper());
            };


            return user.UserName;
        }

        public static string GetFullName(this IIdentity identity)
        {
            var claimIdent = identity as ClaimsIdentity;                        

                return claimIdent != null && claimIdent.HasClaim(c => c.Type == "FullName")
                    ? claimIdent.FindFirst("FullName").Value
                    : string.Empty;
        }

        public static string GetPostalCode(this IIdentity identity)
        {
            var claimIdent = identity as ClaimsIdentity;

            return claimIdent != null && claimIdent.HasClaim(c => c.Type == "PostalCode")
                ? claimIdent.FindFirst("PostalCode").Value
                : string.Empty;
        }

        public static string GetBirthday(this IIdentity identity)
        {
            var claimIdent = identity as ClaimsIdentity;

            return claimIdent != null && claimIdent.HasClaim(c => c.Type == "Birthday")
                ? claimIdent.FindFirst("Birthday").Value
                : string.Empty;
        }

        public static string GetFavouriteClubID(this IIdentity identity)
        {
            var claimIdent = identity as ClaimsIdentity;

            return claimIdent != null && claimIdent.HasClaim(c => c.Type == "FavouriteClub")
                ? claimIdent.FindFirst("FavouriteClub").Value
                : string.Empty;
        }

        public static string GetProfilePic(this IIdentity identity)
        {
            var user = new ApplicationUser();

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var users = db.Users.ToList();
                user = users.FirstOrDefault(u => u.Id.ToUpper() == identity.GetUserId().ToUpper());
            };


            return user.ProfilePic;
        }

        public static string GetBankBalance(this IIdentity identity)
        {

            var user = new ApplicationUser();        
            
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var users = db.Users.ToList();
                user = users.FirstOrDefault(u => u.Id == identity.GetUserId());
            };


            return Math.Round(user.BankAccount, 2).ToString();
        }

    }
}