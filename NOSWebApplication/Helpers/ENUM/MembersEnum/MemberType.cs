﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NOSWebApplication.Helpers.ENUM.MembersEnum
{
    public enum MemberType
    {
        Coach,
        Staff,
        Goalkeeper,
        Defender,
        Midfielder,
        Foward
    }
}