﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NOSWebApplication.Models;

namespace NOSWebApplication.Controllers
{
    public class SeasonsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Seasons
        public ActionResult Index()
        {
            return View(db.SeasonModels.ToList());
        }

        // GET: Seasons/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeasonModel seasonModel = db.SeasonModels.Find(id);
            if (seasonModel == null)
            {
                return HttpNotFound();
            }
            return View(seasonModel);
        }

        // GET: Seasons/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Seasons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idSeason,StartDate,EndDate")] SeasonModel seasonModel)
        {
            if (ModelState.IsValid)
            {
                db.SeasonModels.Add(seasonModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(seasonModel);
        }

        // GET: Seasons/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeasonModel seasonModel = db.SeasonModels.Find(id);
            if (seasonModel == null)
            {
                return HttpNotFound();
            }
            return View(seasonModel);
        }

        // POST: Seasons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idSeason,StartDate,EndDate")] SeasonModel seasonModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(seasonModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(seasonModel);
        }

        // GET: Seasons/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeasonModel seasonModel = db.SeasonModels.Find(id);
            if (seasonModel == null)
            {
                return HttpNotFound();
            }
            return View(seasonModel);
        }

        // POST: Seasons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SeasonModel seasonModel = db.SeasonModels.Find(id);
            db.SeasonModels.Remove(seasonModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
