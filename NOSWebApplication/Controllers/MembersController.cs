﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NOSWebApplication.Models;

namespace NOSWebApplication.Controllers
{
    public class MembersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Members
        public ActionResult Index()
        {
            var memberModels = db.MemberModels.Include(m => m.Club);
            return View(memberModels.ToList());
        }

        // GET: Members/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MemberModel memberModel = db.MemberModels.Find(id);
            if (memberModel == null)
            {
                return HttpNotFound();
            }
            return View(memberModel);
        }

        // GET: Members/Create
        public ActionResult Create()
        {
            ViewBag.idClub = new SelectList(db.ClubModels, "idClub", "Name");
            return View();
        }

        // POST: Members/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idMemeber,Name,MemberType,idClub")] MemberModel memberModel)
        {
            if (ModelState.IsValid)
            {
                db.MemberModels.Add(memberModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idClub = new SelectList(db.ClubModels, "idClub", "Name", memberModel.idClub);
            return View(memberModel);
        }

        // GET: Members/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MemberModel memberModel = db.MemberModels.Find(id);
            if (memberModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.idClub = new SelectList(db.ClubModels, "idClub", "Name", memberModel.idClub);
            return View(memberModel);
        }

        // POST: Members/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idMemeber,Name,MemberType,idClub")] MemberModel memberModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(memberModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idClub = new SelectList(db.ClubModels, "idClub", "Name", memberModel.idClub);
            return View(memberModel);
        }

        // GET: Members/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MemberModel memberModel = db.MemberModels.Find(id);
            if (memberModel == null)
            {
                return HttpNotFound();
            }
            return View(memberModel);
        }

        // POST: Members/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MemberModel memberModel = db.MemberModels.Find(id);
            db.MemberModels.Remove(memberModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
