﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NOSWebApplication.Models;

namespace NOSWebApplication.Controllers
{
    public class ClubsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Clubs
        public ActionResult Index()
        {
            return View(db.ClubModels.ToList());
        }

        // GET: Clubs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClubModel clubModel = db.ClubModels.Find(id);
            if (clubModel == null)
            {
                return HttpNotFound();
            }
            return View(clubModel);
        }

        // GET: Clubs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clubs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idClub,Name,Stadium,Description,Logo")] ClubModel clubModel)
        {
            if (ModelState.IsValid)
            {
                db.ClubModels.Add(clubModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(clubModel);
        }

        // GET: Clubs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClubModel clubModel = db.ClubModels.Find(id);
            if (clubModel == null)
            {
                return HttpNotFound();
            }
            return View(clubModel);
        }

        // POST: Clubs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idClub,Name,Stadium,Description,Logo")] ClubModel clubModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(clubModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(clubModel);
        }

        // GET: Clubs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClubModel clubModel = db.ClubModels.Find(id);
            if (clubModel == null)
            {
                return HttpNotFound();
            }
            return View(clubModel);
        }

        // POST: Clubs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ClubModel clubModel = db.ClubModels.Find(id);
            db.ClubModels.Remove(clubModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
