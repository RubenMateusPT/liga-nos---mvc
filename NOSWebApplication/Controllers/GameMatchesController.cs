﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NOSWebApplication.Models;

namespace NOSWebApplication.Controllers
{
    public class GameMatchesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: GameMatches
        public ActionResult Index()
        {
            var gameMatchModels = db.GameMatchModels.Include(g => g.AwayClub).Include(g => g.Fixture).Include(g => g.HomeClub);
            return View(gameMatchModels.ToList());
        }

        // GET: GameMatches/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GameMatchModel gameMatchModel = db.GameMatchModels.Find(id);
            if (gameMatchModel == null)
            {
                return HttpNotFound();
            }
            return View(gameMatchModel);
        }

        // GET: GameMatches/Create
        public ActionResult Create()
        {
            ViewBag.idAwayClub = new SelectList(db.ClubModels, "idClub", "Name");
            ViewBag.idFixture = new SelectList(db.FixtureModels, "idFixture", "idFixture");
            ViewBag.idHomeClub = new SelectList(db.ClubModels, "idClub", "Name");
            return View();
        }

        // POST: GameMatches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idGameMatch,idFixture,idHomeClub,idAwayClub,HomeGoals,AwayGoals")] GameMatchModel gameMatchModel)
        {
            if (ModelState.IsValid)
            {
                db.GameMatchModels.Add(gameMatchModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idAwayClub = new SelectList(db.ClubModels, "idClub", "Name", gameMatchModel.idAwayClub);
            ViewBag.idFixture = new SelectList(db.FixtureModels, "idFixture", "idFixture", gameMatchModel.idFixture);
            ViewBag.idHomeClub = new SelectList(db.ClubModels, "idClub", "Name", gameMatchModel.idHomeClub);
            return View(gameMatchModel);
        }

        // GET: GameMatches/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GameMatchModel gameMatchModel = db.GameMatchModels.Find(id);
            if (gameMatchModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.idAwayClub = new SelectList(db.ClubModels, "idClub", "Name", gameMatchModel.idAwayClub);
            ViewBag.idFixture = new SelectList(db.FixtureModels, "idFixture", "idFixture", gameMatchModel.idFixture);
            ViewBag.idHomeClub = new SelectList(db.ClubModels, "idClub", "Name", gameMatchModel.idHomeClub);
            return View(gameMatchModel);
        }

        // POST: GameMatches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idGameMatch,idFixture,idHomeClub,idAwayClub,HomeGoals,AwayGoals")] GameMatchModel gameMatchModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gameMatchModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idAwayClub = new SelectList(db.ClubModels, "idClub", "Name", gameMatchModel.idAwayClub);
            ViewBag.idFixture = new SelectList(db.FixtureModels, "idFixture", "idFixture", gameMatchModel.idFixture);
            ViewBag.idHomeClub = new SelectList(db.ClubModels, "idClub", "Name", gameMatchModel.idHomeClub);
            return View(gameMatchModel);
        }

        // GET: GameMatches/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GameMatchModel gameMatchModel = db.GameMatchModels.Find(id);
            if (gameMatchModel == null)
            {
                return HttpNotFound();
            }
            return View(gameMatchModel);
        }

        // POST: GameMatches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GameMatchModel gameMatchModel = db.GameMatchModels.Find(id);
            db.GameMatchModels.Remove(gameMatchModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
