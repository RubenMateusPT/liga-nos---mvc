﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NOSWebApplication.Models;

namespace NOSWebApplication.Controllers
{
    public class FixturesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Fixtures
        public ActionResult Index()
        {
            var fixtureModels = db.FixtureModels.Include(f => f.Season);
            return View(fixtureModels.ToList());
        }

        // GET: Fixtures/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FixtureModel fixtureModel = db.FixtureModels.Find(id);
            if (fixtureModel == null)
            {
                return HttpNotFound();
            }
            return View(fixtureModel);
        }

        // GET: Fixtures/Create
        public ActionResult Create()
        {
            ViewBag.idSeason = new SelectList(db.SeasonModels, "idSeason", "idSeason");
            return View();
        }

        // POST: Fixtures/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idFixture,Date,idSeason")] FixtureModel fixtureModel)
        {
            if (ModelState.IsValid)
            {
                db.FixtureModels.Add(fixtureModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idSeason = new SelectList(db.SeasonModels, "idSeason", "idSeason", fixtureModel.idSeason);
            return View(fixtureModel);
        }

        // GET: Fixtures/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FixtureModel fixtureModel = db.FixtureModels.Find(id);
            if (fixtureModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.idSeason = new SelectList(db.SeasonModels, "idSeason", "idSeason", fixtureModel.idSeason);
            return View(fixtureModel);
        }

        // POST: Fixtures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idFixture,Date,idSeason")] FixtureModel fixtureModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fixtureModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idSeason = new SelectList(db.SeasonModels, "idSeason", "idSeason", fixtureModel.idSeason);
            return View(fixtureModel);
        }

        // GET: Fixtures/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FixtureModel fixtureModel = db.FixtureModels.Find(id);
            if (fixtureModel == null)
            {
                return HttpNotFound();
            }
            return View(fixtureModel);
        }

        // POST: Fixtures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FixtureModel fixtureModel = db.FixtureModels.Find(id);
            db.FixtureModels.Remove(fixtureModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
