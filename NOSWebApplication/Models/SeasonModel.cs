﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NOSWebApplication.Models
{
    public class SeasonModel
    {
        [Key]
        public int idSeason { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public virtual ICollection<FixtureModel> Fixtures { get; set; }
    }
}