﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NOSWebApplication.Models
{
    public class ClubModel
    {
        [Key]
        public int idClub { get; set; }

        [Required(ErrorMessage = "O campo {0} necessita de um nome")]        
        public string Name { get; set; }
        public string Stadium { get; set; }
        public string Description { get; set; }
        public string Logo { get; set; }

        public virtual ICollection<MemberModel> Members { get; set; }

        public virtual ICollection<GameMatchModel> GameMatches { get; set; }
    }
}