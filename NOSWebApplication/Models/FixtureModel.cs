﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NOSWebApplication.Models
{
    public class FixtureModel
    {
        [Key]
        public int idFixture { get; set; }
        public DateTime Date { get; set; }

        public int idSeason { get; set; }
        public virtual SeasonModel Season { get; set; }

        public virtual ICollection<GameMatchModel> GameMatches { get; set; }
    }
}