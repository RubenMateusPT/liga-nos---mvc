﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NOSWebApplication.Models
{
    public class GameMatchModel
    {
        [Key]
        public int idGameMatch { get; set; }

        
        public int idFixture { get; set; }
        public virtual FixtureModel Fixture { get; set; }


        
        public int idHomeClub { get; set; }
        [ForeignKey("idHomeClub")]
        public virtual ClubModel HomeClub { get; set; }

        
        public int idAwayClub { get; set; }
        [ForeignKey("idAwayClub")]
        public virtual ClubModel AwayClub { get; set; }

        public int HomeGoals { get; set; }

        public int AwayGoals { get; set; }
    }
}