﻿using NOSWebApplication.Helpers.ENUM.MembersEnum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NOSWebApplication.Models
{
    public class MemberModel
    {
        [Key]
        public int idMemeber { get; set; }
        public string Name { get; set; }
        public MemberType MemberType { get; set; }

        public int idClub { get; set; }
        public virtual ClubModel Club { get; set; }
    }
}