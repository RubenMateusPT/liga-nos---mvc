﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace NOSWebApplication.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("AzureConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Migrations.Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<GameMatchModel>().HasRequired(c => c.HomeClub).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<GameMatchModel>().HasRequired(c => c.AwayClub).WithMany().WillCascadeOnDelete(false);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<NOSWebApplication.Models.ClubModel> ClubModels { get; set; }

        public System.Data.Entity.DbSet<NOSWebApplication.Models.MemberModel> MemberModels { get; set; }

        public System.Data.Entity.DbSet<NOSWebApplication.Models.FixtureModel> FixtureModels { get; set; }

        public System.Data.Entity.DbSet<NOSWebApplication.Models.GameMatchModel> GameMatchModels { get; set; }

        public System.Data.Entity.DbSet<NOSWebApplication.Models.SeasonModel> SeasonModels { get; set; }
    }
}