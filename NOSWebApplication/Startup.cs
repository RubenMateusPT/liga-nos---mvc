﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NOSWebApplication.Startup))]
namespace NOSWebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
